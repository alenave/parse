package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import model.PaymentDetails;
import model.TransactionSummary;

public class Db {
	static Connection con;

	public static void insertPaymentDetails(List data) throws SQLException {
		PreparedStatement stmt = con
				.prepareStatement("insert into payment_details (neft_id, date, settlement_value) values(?,?,?)");
		Iterator<PaymentDetails> iterator = data.iterator();
		int batchSize = 0;
		while (iterator.hasNext()) {
			PaymentDetails paymentDetails = iterator.next();
			stmt.setString(1, paymentDetails.getNeftId());
			stmt.setString(2, paymentDetails.getPaymentDate());
			stmt.setString(3, paymentDetails.getSettlementValue());
			stmt.addBatch();
			batchSize++;

			if (batchSize % 1000 == 0 || batchSize == data.size()) {
				stmt.executeBatch(); // Execute every 1000 items.
			}
		}
		System.out.println("payment details inserted");
	}

	public static void insertTransactionSummary(List data) throws SQLException {
		PreparedStatement stmt = con.prepareStatement(
				"insert into transaction_summary (order_id, order_item_id, sale_amount, total_offer_amount, my_share, ndd_amount, sdd_amount, customer_shipping_amount, marketplace_fee, taxes, protection_fund, refund) values(?,?,?,?,?,?,?,?,?,?,?,?)");
		Iterator<TransactionSummary> iterator = data.iterator();
		int batchSize = 0;
		while (iterator.hasNext()) {
			TransactionSummary transactionSummary = iterator.next();
			stmt.setString(1, transactionSummary.getOrderId());
			stmt.setString(2, transactionSummary.getOrderItemId());
			stmt.setString(3, transactionSummary.getSaleAmount());
			stmt.setString(4, transactionSummary.getTotalOfferAmount());
			stmt.setString(5, transactionSummary.getMyShare());
			stmt.setString(6, transactionSummary.getNddAmount());
			stmt.setString(7, transactionSummary.getSddAmount());
			stmt.setString(8, transactionSummary.getCustomerShippingAmount());
			stmt.setString(9, transactionSummary.getMarketplaceFee());
			stmt.setString(10, transactionSummary.getTaxes());
			stmt.setString(11, transactionSummary.getProtectionFund());
			stmt.setString(12, transactionSummary.getRefund());
			stmt.addBatch();
			batchSize++;

			if (batchSize % 1000 == 0 || batchSize == data.size()) {
				stmt.executeBatch(); // Execute every 1000 items.
			}
		}
		System.out.println("transaction summary inserted");
	}

	public static void create(List<String> table_details) throws SQLException {

		if (con == null) {
			establishConnection();
		}

		try {
			Class.forName("com.mysql.jdbc.Driver");

			PreparedStatement stmt = con.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `payment_details` (" + "`id` int(11) NOT NULL AUTO_INCREMENT,"
							+ "`neft_id` varchar(45) NOT NULL," + "`date` varchar(45) NOT NULL,"
							+ "`settlement_value` int(11) DEFAULT NULL," + "PRIMARY KEY (`id`),"
							+ "KEY `index_name` (`neft_id`)" + ") ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8");

			stmt.executeUpdate();

			stmt = con.prepareStatement("CREATE TABLE IF NOT EXISTS `transaction_summary` ("
					+ "`id` INT(11) NOT NULL AUTO_INCREMENT," + "`order_id` VARCHAR(45) NOT NULL,"
					+ "`order_item_id` VARCHAR(45) NOT NULL," + "`sale_amount` VARCHAR(45) DEFAULT NULL,"
					+ "`total_offer_amount` VARCHAR(45) NOT NULL," + "`my_share` VARCHAR(45) NOT NULL,"
					+ "`ndd_amount` VARCHAR(45) NOT NULL," + "`sdd_amount` VARCHAR(45) NOT NULL,"
					+ "`customer_shipping_amount` VARCHAR(45) NOT NULL," + "`marketplace_fee` VARCHAR(45) NOT NULL,"
					+ "`taxes` VARCHAR(45) NOT NULL," + "`protection_fund` VARCHAR(45) NOT NULL,"
					+ "`refund` VARCHAR(45) NOT NULL," + "PRIMARY KEY (`id`)," + "KEY `index_name` (`order_id`),"
					+ "KEY `index_mobile` (`order_item_id`)" + ") ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8");

			stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	static void establishConnection() throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/easyecom?useSSL=false", "root", "");
	}

	static void closeConnection() throws SQLException {
		con.close();
	}

}