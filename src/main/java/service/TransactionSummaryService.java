package service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import util.Db;

public class TransactionSummaryService implements Order {

	@Override
	public void parse(Sheet sheet) throws Exception {
		final Properties properties = new Properties();
		properties.load(TransactionSummaryService.class.getResourceAsStream("/application.properties"));

		Iterator<Row> iterator = sheet.iterator();

		List<String> columnNames = new ArrayList<>();
		List<model.TransactionSummary> transactionSummaries = new ArrayList<>();

		columnNames.add(sheet.getSheetName());

		while (iterator.hasNext()) {

			Row currentRow = iterator.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			DataFormatter formatter = new DataFormatter();

			while (cellIterator.hasNext()) {
				Cell currentCell = cellIterator.next();

				if (currentCell.getColumnIndex() > Integer
						.parseInt(properties.getProperty("orders.transaction.summary.data.column.end"))
						&& currentCell.getColumnIndex() < Integer
								.parseInt(properties.getProperty("orders.transaction.summary.data.column.start"))) {
				}

				if (currentCell.getRowIndex() == Integer
						.parseInt(properties.getProperty("orders.transaction.summary.header.main.row"))
						&& currentCell.getColumnIndex() == Integer
								.parseInt(properties.getProperty("orders.transaction.summary.data.column.start"))) {
					columnNames.add(formatter.formatCellValue(currentCell));
				} else if (currentCell.getRowIndex() == Integer
						.parseInt(properties.getProperty("orders.transaction.summary.header.sub.row.start"))
						&& currentCell.getColumnIndex() <= Integer
								.parseInt(properties.getProperty("orders.transaction.summary.data.column.end"))) {
					columnNames.add(formatter.formatCellValue(currentCell));
				} else if (currentCell.getRowIndex() >= Integer
						.parseInt(properties.getProperty("orders.transaction.summary.data.row.start"))
						&& currentCell.getColumnIndex() >= Integer
								.parseInt(properties.getProperty("orders.transaction.summary.data.column.start"))
						&& currentCell.getColumnIndex() <= Integer
								.parseInt(properties.getProperty("orders.transaction.summary.data.column.end"))) {

					model.TransactionSummary transactionSummary = new model.TransactionSummary();
					if (cellIterator.hasNext())
						transactionSummary.setOrderId(formatter.formatCellValue(currentCell));
					if (cellIterator.hasNext())
						transactionSummary.setOrderItemId(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setSaleAmount(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setTotalOfferAmount(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setMyShare(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setNddAmount(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setSddAmount(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setCustomerShippingAmount(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setMarketplaceFee(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setTaxes(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setProtectionFund(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						transactionSummary.setRefund(formatter.formatCellValue(cellIterator.next()));

					transactionSummaries.add(transactionSummary);
				}

			}
		}
		Db.create(columnNames);
		Db.insertTransactionSummary(transactionSummaries);
	}
}