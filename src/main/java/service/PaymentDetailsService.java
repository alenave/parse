package service;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.PaymentDetails;
import util.Db;

public class PaymentDetailsService implements Order {

	@Override
	public void parse(Sheet sheet) throws Exception {
		final Properties properties = new Properties();
		properties.load(PaymentDetailsService.class.getResourceAsStream("/application.properties"));

		Iterator<Row> iterator = sheet.iterator();

		List<String> columnNames = new ArrayList<>();
		List<PaymentDetails> paymentDetails = new ArrayList<>();

		columnNames.add(sheet.getSheetName());

		while (iterator.hasNext()) {

			Row currentRow = iterator.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			DataFormatter formatter = new DataFormatter();

			while (cellIterator.hasNext()) {
				Cell currentCell = cellIterator.next();

				if (currentCell.getColumnIndex() > Integer
						.parseInt(properties.getProperty("orders.payment.details.data.column.end"))) {
					break;
				}

				if (currentCell.getRowIndex() == Integer
						.parseInt(properties.getProperty("orders.payment.details.header.main.row"))
						&& currentCell.getColumnIndex() == Integer
								.parseInt(properties.getProperty("orders.payment.details.data.column.start"))) {
					columnNames.add(formatter.formatCellValue(currentCell));
				} else if (currentCell.getRowIndex() == Integer
						.parseInt(properties.getProperty("orders.payment.details.header.sub.row.start"))
						&& currentCell.getColumnIndex() <= Integer
								.parseInt(properties.getProperty("orders.payment.details.data.column.end"))) {
					columnNames.add(formatter.formatCellValue(currentCell));
				} else if (currentCell.getRowIndex() >= Integer
						.parseInt(properties.getProperty("orders.payment.details.data.row.start"))) {
					PaymentDetails paymentDetailsRow = new PaymentDetails();
					paymentDetailsRow.setNeftId(formatter.formatCellValue(currentCell));
					if (cellIterator.hasNext())
						paymentDetailsRow.setPaymentDate(formatter.formatCellValue(cellIterator.next()));
					if (cellIterator.hasNext())
						paymentDetailsRow.setSettlementValue(formatter.formatCellValue(cellIterator.next()));
					paymentDetails.add(paymentDetailsRow);
				}

			}
		}
		Db.create(columnNames);
		Db.insertPaymentDetails(paymentDetails);
	}
}