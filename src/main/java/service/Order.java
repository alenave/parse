package service;

import org.apache.poi.ss.usermodel.Sheet;

public interface Order {
	void parse(Sheet sheet) throws Exception;
}