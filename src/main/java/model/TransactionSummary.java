package model;

public class TransactionSummary {
	String orderId;

	String orderItemId;

	String saleAmount;

	String totalOfferAmount;

	String myShare;

	String nddAmount;

	String sddAmount;

	String customerShippingAmount;

	String marketplaceFee;

	String taxes;

	String protectionFund;

	String refund;

	public String getCustomerShippingAmount() {
		return customerShippingAmount;
	}

	public String getMarketplaceFee() {
		return marketplaceFee;
	}

	public String getMyShare() {
		return myShare;
	}

	public String getNddAmount() {
		return nddAmount;
	}

	public String getOrderId() {
		return orderId;
	}

	public String getOrderItemId() {
		return orderItemId;
	}

	public String getProtectionFund() {
		return protectionFund;
	}

	public String getRefund() {
		return refund;
	}

	public String getSaleAmount() {
		return saleAmount;
	}

	public String getSddAmount() {
		return sddAmount;
	}

	public String getTaxes() {
		return taxes;
	}

	public String getTotalOfferAmount() {
		return totalOfferAmount;
	}

	public void setCustomerShippingAmount(String customerShippingAmount) {
		this.customerShippingAmount = customerShippingAmount;
	}

	public void setMarketplaceFee(String marketplaceFee) {
		this.marketplaceFee = marketplaceFee;
	}

	public void setMyShare(String myShare) {
		this.myShare = myShare;
	}

	public void setNddAmount(String nddAmount) {
		this.nddAmount = nddAmount;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}

	public void setProtectionFund(String protectionFund) {
		this.protectionFund = protectionFund;
	}

	public void setRefund(String refund) {
		this.refund = refund;
	}

	public void setSaleAmount(String saleAmount) {
		this.saleAmount = saleAmount;
	}

	public void setSddAmount(String sddAmount) {
		this.sddAmount = sddAmount;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

	public void setTotalOfferAmount(String totalOfferAmount) {
		this.totalOfferAmount = totalOfferAmount;
	}
}
