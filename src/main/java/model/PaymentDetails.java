package model;

public class PaymentDetails {
	String neftId;

	String paymentDate;

	String settlementValue;

	public String getNeftId() {
		return neftId;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public String getSettlementValue() {
		return settlementValue;
	}

	public void setNeftId(String neftId) {
		this.neftId = neftId;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public void setSettlementValue(String settlementValue) {
		this.settlementValue = settlementValue;
	}
}
