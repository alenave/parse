
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import service.PaymentDetailsService;
import service.TransactionSummaryService;

public class Application {
	private static final String FILE_NAME = "/Users/alenave/Downloads/Last_POSTPAID.xlsx";

	public static void main(String[] args) throws IOException {
		final Properties properties = new Properties();
		properties.load(Application.class.getResourceAsStream("/application.properties"));

		FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
		Workbook workbook = new XSSFWorkbook(excelFile);
		
		try {
			new PaymentDetailsService().parse(workbook.getSheetAt(Integer.parseInt(properties.getProperty("orders.sheet"))));
			new TransactionSummaryService().parse(workbook.getSheetAt(Integer.parseInt(properties.getProperty("orders.sheet"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
